<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function contact(MailerInterface $mailer): Response
    {
        $email = (new Email());
        $email->from('pat@gmail.com')
              ->to('admin@iepscf.be')
              ->cc('john@gmail.com')
              ->subject('Demande de distanciel')
              ->html('
                    <h2>Suite à votre demande</h2>
                    <h3>Autorisation</h3>
                    <h4>Fournir des dates</h4>
                    ');
        $mailer->send($email);
        return $this->redirectToRoute('home');
    }

    #[Route('/email', name: 'app_email')]
    public function sendEmail(MailerInterface $mailer, Request $request): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = new Email();
            $email->from('pierre.hardy1999@gmail.com')
                  ->to($contact->getEmail())
                  ->subject($contact->getSubject())
//                  ->text($contact->getMessage());
                  ->html('
                                <p>Bonjour <strong>'.$contact->getFirstName().' '.$contact->getLastname().'</strong></p>
                                <p>Vous avez reçu un message: </p>
                                <p>Sujet: '.$contact->getSubject().'</p>
                                <p>Content: '.$contact->getMessage().'</p>
                  ');
            $mailer->send($email);
            $this->addFlash('success','Votre mail a bien été envoyé');
            return $this->redirectToRoute('home');
        }
        return $this->renderForm('contact/contact.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('template', name: 'app_template')]
    public function template(MailerInterface $mailer, Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = new TemplatedEmail();
            $email->from($contact->getEmail())
                  ->to('admin@iepscf-namur.be')
                  ->subject($contact->getSubject())
                  ->htmlTemplate('contact/email-css-external.html.twig')
                  ->context([
                      'title' => $contact->getSubject(),
                      'message' => $contact->getMessage(),
                      'firstName' => $contact->getFirstName(),
                      'lastName' => $contact->getLastname(),
                  ]);
            $mailer->send($email);
            $this->addFlash('success','Votre email a bien été envoyé');
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('contact/contact.html.twig', [
            'form' => $form
        ]);
    }
}
